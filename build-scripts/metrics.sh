generate_image_metrics() {
  if [ -z "${CI_REGISTRY_IMAGE}" ]; then
    echo "CI_REGISTRY_IMAGE not set, can not inspect image metrics"
    exit 1
  fi

  mkdir -p metrics
  METRICS_FILE=metrics/image_metrics.txt
  cat << EOF > "${METRICS_FILE}"
# TYPE container_total_compressed_bytes gauge
# UNIT container_total_compressed_bytes bytes
# HELP container_total_compressed_bytes Sum of container layer sizes in bytes
# TYPE container_layers gauge
# UNIT container_layers number
# HELP container_layers Number of layers in container
EOF

  local _arch_list
  _arch_list=$(IFS=","; for a in ${ARCH_LIST}; do echo $a; done)
  local _image_manifest
  local _arch_manifest

  # iterate over all images with entries in 'artifacts/final/*'
  for image in artifacts/final/* ; do
  
    tagged_image="$(cat "${image}")"
    echo "Collecting ${tagged_image} metrics" >&2 
    # fetch data with skopeo
    _image_manifest=$(skopeo inspect --raw "docker://${CI_REGISTRY_IMAGE}/${tagged_image}")
    mediaType=$(echo "${_image_manifest}" | jq -r ".mediaType")
    echo "mediaType: ${mediaType}"

    if [ "$mediaType" = "application/vnd.oci.image.index.v1+json" -o "$mediaType" = "application/vnd.docker.distribution.manifest.list.v2+json" ]; then
      # for _arch in ${_arch_list[@]}
      for _arch in ${_arch_list}
      do
        sha_list=$(echo "${_image_manifest}" | jq -r --arg arch "$_arch" '.manifests[] | select (.platform.architecture == $arch) | .digest')
        for sha in ${sha_list}
        do
          image_name=${tagged_image%%:*}
          _arch_manifest=$(skopeo inspect --raw "docker://${CI_REGISTRY_IMAGE}/${image_name}@${sha}" )
          layers=$(echo "${_arch_manifest}" | jq -r '.layers | length')
          total_size=$(echo "${_arch_manifest}" | jq -r '[ .layers[].size ] | add')
          cat << EOF >> "${METRICS_FILE}"
container_layers{image="${tagged_image}-${_arch}"} ${layers}
container_total_compressed_bytes{image="${tagged_image}-${_arch}"} ${total_size}
EOF
        done
      done
    else
      # must be a single arch if we're here
      local _arch=${ARCH_LIST%%,*}
      # collect desired data
      layers=$(echo ${_image_manifest} | jq -r '.layers | length')
      total_size=$(echo ${_image_manifest} | jq -r '[ .layers[].size ] | add')
      # append to METRICS_FILE
      cat << EOF >> "${METRICS_FILE}"
container_layers{image="${tagged_image}-${_arch}"} ${layers}
container_total_compressed_bytes{image="${tagged_image}-${_arch}"} ${total_size}
EOF
    fi

  done

  # terminate the file according to specification. '# EOF\n'
  echo '# EOF' >> "${METRICS_FILE}"
}
